var Khongdau = require("khong-dau");

function handleArray(arr) {
	var newArr = [];

	for (let index = 0; index < arr.length; index++) {
		newArr[index] = {
			name: Khongdau(arr[index].name.toLowerCase()),
			phone: arr[index].phone
		};
	}

	return newArr;
}

module.exports = handleArray;
