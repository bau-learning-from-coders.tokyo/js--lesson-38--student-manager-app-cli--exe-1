// import package
var fs = require("fs");
var readlineSync = require("readline-sync");
var KhongDau = require("khong-dau");
var handleArray = require("./handleArray");
// create some variables
var data = JSON.parse(fs.readFileSync("./data.json"));
var fakeData = [];

// lower and convert text, then create a new array
fakeData = handleArray(data);

key = 1;

while (key != 5) {
	// ! First step: choose what you wanna do:
	var key = readlineSync.questionInt(`What do you want from me:
1. Send It To My Phone - Austin Mahone
2. Somebody That I Used To Know - Gotye
3. Erase You - ESG
4. The Phone Book - Editors
5. Goodbye My Lover - James Blunt
Your answer is `);
	console.log("-----------");

	// ! Second step: make your request
	if (key == 1) {
		// get the new member
		let name = readlineSync.question("Your name, pls?");
		let phone = readlineSync.questionInt("and Your phone number, pls? ");

		// add him/her to the list and show the new data
		data.push({ name, phone });
		fakeData = handleArray(data);
		console.log("----------");
		console.log("Data is updated");
		console.log("the new data will be like:");
		console.log(data);
		console.log("--------");

		fs.writeFileSync("./newData.json", JSON.stringify(data));
	} else if (key == 2) {
		// show list friends and select one
		console.log(`List of your friends: `);
		for (let index = 0; index < data.length; index++) {
			console.log(`${index + 1}. ${data[index].name}`);
		}

		// get the friend and change her/his info
		key = readlineSync.questionInt(
			`The number of her/him? Your answer is: `
		);

		let name = readlineSync.question("His/her new name, pls? ");
		let phone = readlineSync
			.questionInt("and His/Her new phone number, pls? ")
			.toString();

		data[key - 1] = { name, phone };

		// log the new data
		console.log(`The new list will be like:`);
		console.log(data);
		console.log("--------");
	} else if (key == 3) {
		// show list friends
		console.log(`List of your friends: `);
		for (let index = 0; index < data.length; index++) {
			console.log(`${index + 1}. ${data[index].name}`);
		}

		// get the position of the one you want to erase and then, make her/his color faked
		key = readlineSync.questionInt(
			`The number of her/him? Your answer is: `
		);
		data.splice(key - 1, 1);

		// show new data
		console.log("The new list will be like:");
		for (let index = 0; index < data.length; index++) {
			console.log(`${index + 1}. ${data[index].name}`);
		}
		console.log("------");
	} else if (key == 4) {
		key = KhongDau(
			readlineSync
				.question(`Name or his/her numbers that you can remember: `)
				.toLowerCase()
		);

		// create an array to keep the info, and then start to search
		var listOfYourFriendsThatICanFind = [];

		for (let j = 0; j < fakeData.length; j++) {
			if (
				fakeData[j].name.indexOf(key) > -1 ||
				fakeData[j].phone.indexOf(key) > -1
			)
				listOfYourFriendsThatICanFind.push({
					name: data[j].name,
					phone: data[j].phone
				});
		}

		console.log(listOfYourFriendsThatICanFind);
		console.log("---------");
	} else {
		console.log("Bye Bye");
	}
}
